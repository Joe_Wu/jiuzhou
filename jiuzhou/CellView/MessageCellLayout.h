//
//  ActivityCell.h
//  HiYoungMan
//
//  Created by YangJoe on 3/28/14.
//  Copyright (c) 2014 YangJoe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PAImageView.h"

@interface MessageCellLayout : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *CellBackView;

@end
