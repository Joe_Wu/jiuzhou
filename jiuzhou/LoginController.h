//
//  ViewController.h
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKNavigationController.h"

@interface LoginController : UIViewController<UITextFieldDelegate>{
    UITextField *UserName;
    UITextField *Password;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KKNavigationController *navController;
@end

