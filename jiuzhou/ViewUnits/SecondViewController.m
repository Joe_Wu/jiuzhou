//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "SecondViewController.h"
#import "ColorFont.h"
#import "ChineseString.h"

#import "HeadView.h"
#import "PAImageView.h"

#import "ChatViewController.h"
@interface SecondViewController ()
@end

@implementation SecondViewController
@synthesize indexArray;
@synthesize LetterResultArr;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    contents = [[NSMutableArray alloc]init];
    // 1 create top view
    [self CreateTopView];
    
    [self CreateSegment];
    
    [self CreateTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 1 create top view
#pragma mark - 创建顶部视图
-(void)CreateTopView{
    UIView *payView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 64)];
    payView.backgroundColor = [UIFont MainColor];
    [self.view addSubview:payView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, 40)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont titleFont];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"通讯录";
    [payView addSubview:titleLabel];
}

// 2 Create Segment
-(void)CreateSegment{
    // ===================添加选择栏=============start
    NSArray *segmentedArray = [[NSArray alloc]initWithObjects:@"所有",@"部门",nil];
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentedArray];
    segmentedControl.frame = CGRectMake(self.view.frame.size.width/4, 70, self.view.frame.size.width/2, 40.0);
    segmentedControl.selectedSegmentIndex = 0;
    [segmentedControl setTintColor:[UIColor grayColor]];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],UITextAttributeTextColor,  [UIFont fontWithName:@"Avenir-Book" size:15.f],UITextAttributeFont ,[UIColor blackColor],UITextAttributeTextShadowColor ,nil];
    [segmentedControl setTitleTextAttributes:dic forState:UIControlStateNormal];
    
    [segmentedControl addTarget:self action:@selector(segmentAction:)forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    //===================添加选择栏=============start
}
-(void)segmentAction:(UISegmentedControl *)Seg{
    
    NSInteger Index = Seg.selectedSegmentIndex;
    if (Index == 0) {
        [_tableView removeFromSuperview];
        _tableView = nil;
        isAll =false;
        if (!_tableView) {
            [self CreateTable];
        }
    }else if(Index == 1){
        [_tableView removeFromSuperview];
        _tableView = nil;
        isAll =true;
        if (!_tableView) {
            [self CreatePopDownTabel];
        }
    }else{
        
    }
}


// 3 Create content view
-(void)CreateTable{
    NSArray *stringsToSort=[NSArray arrayWithObjects:
                            @"王小妹",@" ￥阿妈",@"中国执照",@"李晓华",
                            @"吴澳",@"社交圈",@"开发者",@"不知道",
                            @"奥巴马",@"习近平",@"李克强",@"张国立",@"周杰伦",
                            @"王尼玛", @"我的鼠标",@"Meachel",@"John",
                            nil];
    
    self.indexArray = [ChineseString IndexArray:stringsToSort];
    self.LetterResultArr = [ChineseString LetterSortArray:stringsToSort];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 110,self.view.frame.size.width,self.view.frame.size.height-64) style:UITableViewStyleGrouped];
    _tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, 0.01)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
}

#pragma mark -Section的Header的值
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (isAll) {
        return nil;
    }else{
        NSString *key = [indexArray objectAtIndex:section];
        return key;
    }
    
}
#pragma mark - Section header view
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (isAll) {
        return [self.headViewArray objectAtIndex:section];
    }else{
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        lab.backgroundColor = [UIColor grayColor];
        lab.text = [indexArray objectAtIndex:section];
        lab.textColor = [UIColor whiteColor];
        return lab;
    }
    
}
#pragma mark - row height
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isAll) {
        HeadView* headView = [self.headViewArray objectAtIndex:indexPath.section];
        return headView.open?60:0;
    }else{
        return 65.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (isAll) {
        return 60;
    }else{
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark -
#pragma mark Table View Data Source Methods
#pragma mark -设置右方表格的索引数组
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isAll) {
        return nil;
    }else{
        return indexArray;
    }
    
}
#pragma mark -
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    NSLog(@"title===%@",title);
    return index;
}


#pragma mark -允许数据源告知必须加载到Table View中的表的Section数。
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isAll) {
        return [self.headViewArray count];
    }else{
        return [indexArray count];
    }
}
#pragma mark -设置表格的行数为数组的元素个数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isAll) {
        HeadView* headView = [self.headViewArray objectAtIndex:section];
        return headView.open?5:0;
    }else{
        return [[self.LetterResultArr objectAtIndex:section] count];
    }
    
}
#pragma mark -每一行的内容为数组相应索引的值
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isAll) {
        static NSString *indentifier = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
            UIButton* backBtn=  [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
            backBtn.tag = 20000;
//            [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_on"] forState:UIControlStateHighlighted];
            backBtn.userInteractionEnabled = NO;
            [cell.contentView addSubview:backBtn];
            
            UIImageView* line = [[UIImageView alloc]initWithFrame:CGRectMake(0, 59.5, self.view.frame.size.width, 0.5)];
            line.backgroundColor = [UIColor grayColor];
            [cell.contentView addSubview:line];
            
        }
        UIButton* backBtn = (UIButton*)[cell.contentView viewWithTag:20000];
        HeadView* view = [self.headViewArray objectAtIndex:indexPath.section];
//        [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_2_nomal"] forState:UIControlStateNormal];
        backBtn.titleLabel.textColor = [UIColor blackColor];
        
        if (view.open) {
            if (indexPath.row == _currentRow) {
//                [backBtn setBackgroundImage:[UIImage imageNamed:@"btn_nomal"] forState:UIControlStateNormal];
            }
        }
        
        PAImageView *asynImgView = [[PAImageView alloc]initWithFrame2:CGRectMake(10, 10, 40, 40) backgroundProgressColor:[UIColor whiteColor] progressColor:[UIColor lightGrayColor]];
        [cell addSubview:asynImgView];
        [asynImgView setImageURL:@"http://e.hiphotos.baidu.com/image/h%3D200/sign=dca15b374f086e0675a8384b32087b5a/5ab5c9ea15ce36d3b101443639f33a87e950b1ab.jpg"];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 10, self.view.frame.size.width-70, 40)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = @"王力宏";
        [cell addSubview:titleLabel];
        
//        cell.textLabel.text = [NSString stringWithFormat:@"%d-%d",indexPath.section,indexPath.row];
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//        cell.textLabel.textColor = [UIColor whiteColor];
        
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.textLabel.text = [[self.LetterResultArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        return cell;
    }
}
#pragma mark - Select内容为数组相应索引的值
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isAll) {
        _currentRow = indexPath.row;
        [_tableView reloadData];
    }else{
        ChatViewController *menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        menuVc.ChatName = [[self.LetterResultArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:(UIViewController*)menuVc animated:YES];

    }
   
}




// 4 pop down table
-(void)CreatePopDownTabel{
    [self loadModel];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 110,self.view.frame.size.width,self.view.frame.size.height-110) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}


- (void)loadModel{
    _currentRow = -1;
    _headViewArray = [[NSMutableArray alloc]init ];
    for(int i = 0;i< 5 ;i++)
    {
        HeadView* headview = [[HeadView alloc] initWithFrame:self.view.frame];
        headview.delegate = self;
        headview.section = i;
        [headview.backBtn setTitle:[NSString stringWithFormat:@"第%d组",i] forState:UIControlStateNormal];
        [self.headViewArray addObject:headview];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _tableView= nil;
}

#pragma mark - TableViewdelegate&&TableViewdataSource

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}



#pragma mark - HeadViewdelegate
-(void)selectedWith:(HeadView *)view{
    _currentRow = -1;
    if (view.open) {
        for(int i = 0;i<[_headViewArray count];i++)
        {
            HeadView *head = [_headViewArray objectAtIndex:i];
            head.open = NO;
            [head.tagBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
        }
        [_tableView reloadData];
        return;
    }
    _currentSection = view.section;
    [self reset];
    
}

//界面重置
- (void)reset
{
    for(int i = 0;i<[_headViewArray count];i++)
    {
        HeadView *head = [_headViewArray objectAtIndex:i];
        if(head.section == _currentSection)
        {
            head.open = YES;
            [head.tagBtn setBackgroundImage:[UIImage imageNamed:@"btn_on"] forState:UIControlStateNormal];
        }else {
            [head.tagBtn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
            head.open = NO;
        }
    }
    [_tableView reloadData];
}





@end
