//
//  ViewController.m
//  MallsDisplay
//
//  Created by Yang Joe on 3/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "ModifyPasswordView.h"
#import "ColorFont.h"
#import "PAImageView.h"
#import "TouchUIView.h"
#import "GUAAlertView.h"
//#import <SMS_SDK/SMS_SDK.h>

@interface ModifyPasswordView ()

@end

@implementation ModifyPasswordView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:83.0/255.0 blue:109.0/255.0 alpha:1.0f];
    textFiledArray = [[NSMutableArray alloc]init];
    // 1 顶部view
    [self TopView];

    // 2 content view
    [self ListView];
}

// 1 顶部view
#pragma mark - 创建顶部的视图
-(void)TopView{
    UIView *payView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 64)];
    payView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:payView];
    
    UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, 40)];
    priceLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    priceLabel.textAlignment = NSTextAlignmentCenter;
    priceLabel.font = [UIFont titleFont];
    priceLabel.textColor = [UIColor whiteColor];
    priceLabel.text = @"修改密码";
    [payView addSubview:priceLabel];
    
    TouchUIView *BackView2=[[TouchUIView alloc] initWithFrame:CGRectMake(10,30,50, 20)];
    BackView2.dragEnable = YES;
    [BackView2 setBackgroundColor:[UIColor clearColor]];
    BackView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackButton:)];
    [BackView2 addGestureRecognizer:singleTap];
    [payView addSubview:BackView2];
    //image
    UIImageView  *icon=[[UIImageView alloc] initWithFrame:CGRectMake(2, 4, 7, 12)];
    [icon setImage:[UIImage imageNamed:@"back_icon.png"]];
    [BackView2 addSubview:icon];
    // label
    UILabel *TextLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, 40, 20)];
    TextLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    TextLabel.textAlignment = NSTextAlignmentLeft;
    TextLabel.font = [UIFont boldFontName:16];
    TextLabel.textColor = [UIColor whiteColor];
    TextLabel.text = @"返回";
    [BackView2 addSubview:TextLabel];
    
    // search button
    UIButton *SearchButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    SearchButton.frame=CGRectMake(self.view.frame.size.width-50,30,40,20);
    SearchButton.titleLabel.font = [UIFont boldFontName:16];
    [SearchButton setTitle:@"完成" forState:UIControlStateNormal];
    [SearchButton addTarget:self action:@selector(Submit:) forControlEvents:UIControlEventTouchUpInside];
    [SearchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [SearchButton setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [payView addSubview:SearchButton];
    
}
-(void)BackButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)Submit:(id)sender{
//    UITextField *text1 = [textFiledArray objectAtIndex:3];
//    UITextField *text2 = [textFiledArray objectAtIndex:1];
//    UITextField *text3 = [textFiledArray objectAtIndex:2];
//    if ([text2.text isEqualToString:text3.text]) {
//        [SMS_SDK commitVerifyCode:text1.text result:^(enum SMS_ResponseState state) {
//            if (1==state) {
//                
//                NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc]init];
//                [mutableDictionary setObject:text2.text forKey:@"pssword"];
//                
//                NSMutableDictionary *userDic = [[NSMutableDictionary alloc]init];
//                [userDic setObject:mutableDictionary forKey:@"user"];
//                
//                SA = [[ServiceAPI alloc]initWithFrame:self.view.frame];
//                SA.url = @"users.json";
//                SA.delegate = self;
//                [SA SetPutRequest:userDic];
//                [self.view addSubview:SA];
//            }
//            else if(0==state){
//                GUAAlertView *v = [GUAAlertView alertViewWithTitle:@""
//                                                           message:@"验证码不正确"
//                                                       buttonTitle:@"确定"
//                                               buttonTouchedAction:^{
//                                                   
//                                               } dismissAction:^{
//                                               }];
//                
//                [v show];
//            }
//        }];
//    }else{
//        GUAAlertView *v = [GUAAlertView alertViewWithTitle:@""
//                                                   message:@"新密码两次输入不一致"
//                                               buttonTitle:@"确定"
//                                       buttonTouchedAction:^{
//                                           
//                                       } dismissAction:^{
//                                       }];
//        
//        [v show];
//    }
}

// 2 list view
#pragma mark - 创建发现信息列表视图
-(void)ListView{
    listInfoView=[[UIView alloc] initWithFrame:CGRectMake(0,64, self.view.frame.size.width, self.view.frame.size.height - 64)];
    listInfoView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:listInfoView];
    
    NSArray *array = [[NSArray alloc] initWithObjects:@"手机号",@"新密码",@"确认新密码",@"验证码",nil];
    NSArray *tin = [[NSArray alloc] initWithObjects:@"请输入手机号码",@"请输入新密码",@"请确认新密码",@"请输入验证码",nil];
    for (int i = 0; i < array.count; i++) {
        UIView *listview=[[UIView alloc] initWithFrame:CGRectMake(0,14+60*i, self.view.frame.size.width, 60)];
        listview.backgroundColor = [UIColor clearColor];
        [listInfoView addSubview:listview];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(100.0f, 17, self.view.frame.size.width-120, 30.0f)];
        [textField setBorderStyle:UITextBorderStyleNone]; //外框类型
        textField.placeholder = [tin objectAtIndex:i];
        [textField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        [textField setValue:[UIFont boldSystemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.returnKeyType = UIReturnKeyDone;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
        textField.textColor = [UIColor whiteColor];
        textField.delegate = self;
        [listview addSubview:textField];
        [textFiledArray addObject:textField];
        if (i == 0) {
            textField.keyboardType = UIKeyboardTypeDecimalPad;//键盘类型为数字
        }
        
        UILabel *TextLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(10, (listview.frame.size.height-20)/2, 100, 20)];
        TextLabel1.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        TextLabel1.textAlignment = NSTextAlignmentLeft;
        TextLabel1.font = [UIFont fontName:16];
        TextLabel1.textColor = [UIColor whiteColor];
        TextLabel1.text = [array objectAtIndex:i];
        [listview addSubview:TextLabel1];
        
        
        UIView *line2=[[UIView alloc] initWithFrame:CGRectMake(0,listview.frame.size.height-0.5, self.view.frame.size.width, 0.5)];
        line2.backgroundColor = [UIColor colorWithRed:16.0/255 green:16.0/255 blue:16.0/255 alpha:1.0f];
        [listview addSubview:line2];
    }
    
    UIButton *RegirestBut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    RegirestBut.frame = CGRectMake(0, 290, self.view.frame.size.width, 50.0f);
    RegirestBut.backgroundColor = [UIColor colorWithRed:148.0/255 green:87.0/255 blue:145.0/255 alpha:1.0f];
    RegirestBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:18.0f];
    [RegirestBut setTitle:@"获取验证码" forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [RegirestBut addTarget:self action:@selector(GetSecretCode:) forControlEvents:UIControlEventTouchUpInside];
    [listInfoView addSubview:RegirestBut];
}
-(void)GetSecretCode:(id)sender{
//    UITextField *text1 = [textFiledArray objectAtIndex:0];
//    [SMS_SDK getVerifyCodeByPhoneNumber:text1.text AndZone:@"86" result:^(enum SMS_GetVerifyCodeResponseState state) {
//        if (1==state) {
//            NSLog(@"block 获取验证码成功");
//        }
//        else if(0==state)
//        {
//            NSLog(@"block 获取验证码失败");
//        }
//        else if (SMS_ResponseStateMaxVerifyCode==state)
//        {
//            
//        }
//        else if(SMS_ResponseStateGetVerifyCodeTooOften==state)
//        {
//            
//        }
//    }];
}

// response
-(void)RequestFaild:(NSString *)url{
    NSLog(@"faild");
    [SA removeFromSuperview];
}
-(void)RequestSuccess:(NSMutableDictionary *)ResultString{
    [SA removeFromSuperview];
    
    NSLog(@"success-%@",ResultString);
    NSString *stateCode = [ResultString objectForKey:@"stateCode"];
    if ([stateCode isEqualToString:@"200"]) {
        GUAAlertView *v = [GUAAlertView alertViewWithTitle:@""
                                                   message:@"修改密码成功"
                                               buttonTitle:@"确定"
                                       buttonTouchedAction:^{
                                           
                                           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                           [userDefaults setObject:@"1" forKey:@"modifypassword"];
                                           
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       } dismissAction:^{
                                           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                           [userDefaults setObject:@"1" forKey:@"modifypassword"];
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }];
        
        [v show];
    }else{
        GUAAlertView *v = [GUAAlertView alertViewWithTitle:@""
                                                   message:@"修改密码失败"
                                               buttonTitle:@"确定"
                                       buttonTouchedAction:^{
                                       } dismissAction:^{
                                       }];
        
        [v show];
    }
}

@end
