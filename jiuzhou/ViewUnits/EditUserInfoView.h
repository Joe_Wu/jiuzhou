//
//  ViewController.h
//  MallsDisplay
//
//  Created by Yang Joe on 3/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"
#import "ModifyPasswordView.h"

@protocol RefreshUserInfoDelegate <NSObject>

-(void)RefreshUserInfo:(NSString*)UserName PhoneNumber:(NSString*)phoneNumber;

@end

@interface EditUserInfoView : UIViewController<UITextViewDelegate,UITextFieldDelegate,ServiceAPIDelegate>{
    UILabel *textViewlabel;
    UIView *listInfoView;
    ServiceAPI *SA;
    NSMutableArray *textFiledArray;
}
@property (nonatomic, retain) id <RefreshUserInfoDelegate> delegate;
@end

