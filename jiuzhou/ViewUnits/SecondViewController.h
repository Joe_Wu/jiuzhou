//
//  ViewController.h
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeadView.h"

@interface SecondViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,HeadViewDelegate>{
    BOOL *isAll;
    NSMutableArray *contents;
    
    
    UITableView* _tableView;
    NSInteger _currentSection;
    NSInteger _currentRow;
}

@property(nonatomic,retain)NSMutableArray *indexArray;
//设置每个section下的cell内容
@property(nonatomic,retain)NSMutableArray *LetterResultArr;

@property(nonatomic, retain) UITableView* tableView;



@property(nonatomic, retain) NSMutableArray* headViewArray;
@end

