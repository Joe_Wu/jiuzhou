//
//  ViewController.h
//  MallsDisplay
//
//  Created by Yang Joe on 3/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceAPI.h"

@interface ModifyPasswordView : UIViewController<UITextViewDelegate,UITextFieldDelegate,ServiceAPIDelegate>{
    UILabel *textViewlabel;
    UIView *listInfoView;
    ServiceAPI *SA;
    NSMutableArray *textFiledArray;
    
}
@end

