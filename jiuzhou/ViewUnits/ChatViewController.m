//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "ChatViewController.h"
#import "ColorFont.h"
#import "TouchUIView.h"
@interface ChatViewController ()
@end

@implementation ChatViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0f];
    
    // 1 create top view
    [self CreateTopView];
    
    [self ChatView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 1 create top view
#pragma mark - 创建顶部视图
-(void)CreateTopView{
    UIView *payView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 64)];
    payView.backgroundColor = [UIFont MainColor];
    [self.view addSubview:payView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, 40)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont titleFont];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = _ChatName;
    [payView addSubview:titleLabel];
    
    TouchUIView *BackView2=[[TouchUIView alloc] initWithFrame:CGRectMake(10,30,50, 20)];
    BackView2.dragEnable = YES;
    [BackView2 setBackgroundColor:[UIColor clearColor]];
    BackView2.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackButton:)];
    [BackView2 addGestureRecognizer:singleTap];
    [payView addSubview:BackView2];
    //image
    UIImageView  *icon=[[UIImageView alloc] initWithFrame:CGRectMake(2, 4, 7, 12)];
    [icon setImage:[UIImage imageNamed:@"back_icon.png"]];
    [BackView2 addSubview:icon];
    // label
    UILabel *TextLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, 40, 20)];
    TextLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    TextLabel.textAlignment = NSTextAlignmentLeft;
    TextLabel.font = [UIFont boldFontName:16];
    TextLabel.textColor = [UIColor whiteColor];
    TextLabel.text = @"返回";
    [BackView2 addSubview:TextLabel];
}
-(void)BackButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

// 2 Create Textfiled View
-(void)ChatView{
    ChatTextflied = [[UITextField alloc] initWithFrame:CGRectMake(2.0f, self.view.frame.size.height-42, self.view.frame.size.width-65, 40.0f)];
    [ChatTextflied setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    ChatTextflied.backgroundColor = [UIColor whiteColor];
    ChatTextflied.layer.cornerRadius = 3.0f;
    ChatTextflied.placeholder = @"";
    ChatTextflied.delegate =self;
    ChatTextflied.keyboardType = UIKeyboardTypeDefault;
    ChatTextflied.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    ChatTextflied.autocorrectionType = UITextAutocorrectionTypeNo;
    ChatTextflied.autocapitalizationType = UITextAutocapitalizationTypeNone;
    ChatTextflied.returnKeyType = UIReturnKeyDone;
    ChatTextflied.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
    [self.view addSubview:ChatTextflied];
    
    
    UIButton *RegirestBut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    RegirestBut.frame = CGRectMake(self.view.frame.size.width - 62, self.view.frame.size.height-42, 60, 40.0f);
    RegirestBut.backgroundColor = [UIColor colorWithRed:101.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1.0f];
    RegirestBut.layer.cornerRadius = 3.0f;
    RegirestBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:18.0f];
    [RegirestBut setTitle:@"发送" forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [RegirestBut addTarget:self action:@selector(SendMessage:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:RegirestBut];
    
    //hide the virtual keyboad
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
}
-(void)SendMessage:(id)sender{
    
}


// ------输入时，修改平的高度----start
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [ChatTextflied resignFirstResponder];
    return YES;
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [ChatTextflied resignFirstResponder];
}
// change the location when inputing
- (void)moveView:(UITextField *)textField leaveView:(BOOL)leave{
    float screenHeight,keyboardHeight;

    screenHeight = self.view.frame.size.height; //屏幕尺寸，如果屏幕允许旋转，可根据旋转动态调整
    keyboardHeight = 430; //键盘尺寸，如果屏幕允许旋转，可根据旋转动态调整
    
    float statusBarHeight,NavBarHeight,tableCellHeight,textFieldOriginY,textFieldFromButtomHeigth;
    int margin;
    statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height; //屏幕状态栏高度
    NavBarHeight = self.navigationController.navigationBar.frame.size.height; //获取导航栏高度
    
    UITableViewCell *tableViewCell=(UITableViewCell *)textField.superview;
    tableCellHeight = tableViewCell.frame.size.height; //获取单元格高度
    
    CGRect fieldFrame=[self.view convertRect:textField.frame fromView:tableViewCell];
    textFieldOriginY = fieldFrame.origin.y; //获取文本框相对本视图的y轴位置。
    
    //计算文本框到屏幕底部的高度（屏幕高度-顶部状态栏高度-导航栏高度-文本框的的相对y轴位置-单元格高度）
//    textFieldFromButtomHeigth = screenHeight - statusBarHeight - NavBarHeight - textFieldOriginY - tableCellHeight;
    textFieldFromButtomHeigth = screenHeight - textFieldOriginY;
    NSLog(@"%f",textFieldFromButtomHeigth);
    
    if(!leave) {
        if(textFieldFromButtomHeigth < keyboardHeight+90) { //如果文本框到屏幕底部的高度 < 键盘高度
            margin = keyboardHeight - textFieldFromButtomHeigth+90; // 则计算差距
            keyBoardMargin_ = margin; //keyBoardMargin_ 为成员变量，记录上一次移动的间距,用户离开文本时恢复视图高度
        } else {
            margin= 0;
            keyBoardMargin_ = 0;
        }
    }
    
    float movementDuration = 0.3f; // 动画时间
    
    int movement;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (deviceOrientation == 0) {
        movement = (leave ? keyBoardMargin_ : -margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }else{
        movement = (leave ? -keyBoardMargin_ : margin); //进入时根据差距移动视图，离开时恢复之前的高度
    }
    
    [UIView beginAnimations: @"textFieldAnim" context: nil]; //添加动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.view.frame = CGRectOffset(self.view.frame, movement/8, 0);
    }else{
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement/2);
    }
    [UIView commitAnimations];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self moveView:textField leaveView:NO];
}
- (void)textFieldDidEndEditing:(UITextField *)textField;{
    [self moveView:textField leaveView:YES];
}
// ------输入时，修改平的高度----start
@end
