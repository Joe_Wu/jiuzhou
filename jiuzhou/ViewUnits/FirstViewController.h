//
//  ViewController.h
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
// SEARCH
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
#import "PushView.h"

@interface FirstViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    NSMutableArray *dataArray;
    NSMutableArray *searchResults;
    UISearchBar *mySearchBar;
    UISearchDisplayController *searchDisplayController;
}

@property(nonatomic, retain) UITableView* tableView;
@end

