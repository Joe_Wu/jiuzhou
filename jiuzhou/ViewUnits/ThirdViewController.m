//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "ThirdViewController.h"
#import "ColorFont.h"
#import "TouchUIView.h"
#import "WorkViewController.h"
@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // 1 create top view
    [self CreateADView];
    
    // 1 create content view
    [self CreateContentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 1 create top view
#pragma mark - 创建顶部视图
-(void)CreateADView{
    NSMutableArray *imageAry = [[NSMutableArray alloc]init];
    [imageAry addObject:@"http://img.sccnn.com/bimg/331/269.jpg"];
    [imageAry addObject:@"http://img.sccnn.com/bimg/337/29153.jpg"];
    [imageAry addObject:@"http://pic19.nipic.com/20120315/5607045_180307175000_2.jpg"];
    
    AdScrollView * scrollView = [[AdScrollView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,self.view.frame.size.height/4)];
    scrollView.delegate = self;
    scrollView.imageNameArray = imageAry;
    scrollView.PageControlShowStyle = UIPageControlShowStyleCenter;
    scrollView.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    [scrollView setAdTitleArray:imageAry withShowStyle:AdTitleShowStyleLeft];
    scrollView.pageControl.currentPageIndicatorTintColor = [UIColor purpleColor];
    [self.view addSubview:scrollView];
}

// 1 create content view
#pragma mark - 创建主要视图
-(void)CreateContentView{
    NSMutableArray *colorArray = [[NSMutableArray alloc]init];
    [colorArray addObject:[UIColor colorWithRed:247.0/255 green:77.0/255 blue:77.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:182.0/255 green:107.0/255 blue:24.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:23.0/255 green:166.0/255 blue:224.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:239.0/255 green:137.0/255 blue:89.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:60.0/255 green:89.0/255 blue:45.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:222.0/255 green:71.0/255 blue:142.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:247.0/255 green:77.0/255 blue:77.0/255 alpha:1.0f]];
    [colorArray addObject:[UIColor colorWithRed:182.0/255 green:107.0/255 blue:24.0/255 alpha:1.0f]];
    
    NSArray *imaArr =[[NSArray alloc] initWithObjects:@"main_1.png",@"main_2.png",@"main_3.png",@"main_4.png",@"main_5.png",@"main_6.png",@"main_7.png",@"main_8.png",nil];
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"计划",@"考勤",@"公告",@"客户档案",@"资料库",@"工作日报",@"信息采集",@"下属工作",nil];
    
    for (int i = 0; i < imaArr.count; i++) {
        int index = i%3;
        int row = i/3;
        
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3*index,self.view.frame.size.height/4+30+(self.view.frame.size.width/3-25)*row,self.view.frame.size.width/3+0.5, self.view.frame.size.width/3-25+0.5)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[colorArray objectAtIndex:i]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = i;
        Unit.layer.borderWidth = 5;
        Unit.layer.borderColor = [[UIColor whiteColor] CGColor];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ChooseType:)];
        [Unit addGestureRecognizer:singleTap];
        [self.view addSubview:Unit];
        // image
        UIImageView  *imageView=[[UIImageView alloc] initWithFrame:CGRectMake((Unit.frame.size.width-36)/2, 15, 36, 36)];
        [imageView setImage:[UIImage imageNamed:[imaArr objectAtIndex:i]]];
        [Unit addSubview:imageView];
        // title
        UILabel *TitleLable = [[UILabel alloc]initWithFrame:CGRectMake(10, Unit.frame.size.height-30, Unit.frame.size.width-20, 20)];
        TitleLable.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        TitleLable.textAlignment = NSTextAlignmentCenter;
        TitleLable.font = [UIFont boldFontName:16];
        TitleLable.textColor = [UIColor whiteColor];
        TitleLable.text = [titeArr objectAtIndex:i];
        [Unit addSubview:TitleLable];
    }
}
-(void)ChooseType:(id)sender{
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    int index = (int)[singleTap view].tag;
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"计划",@"考勤",@"公告",@"客户档案",@"资料库",@"工作日报",@"信息采集",@"下属工作",nil];
    
    WorkViewController *menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"WorkViewController"];
    menuVc.ChatName = [titeArr objectAtIndex:index];
    [self.navigationController pushViewController:(UIViewController*)menuVc animated:YES];
}
@end
