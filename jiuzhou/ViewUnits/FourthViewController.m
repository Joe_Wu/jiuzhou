//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "FourthViewController.h"
#import "ColorFont.h"
#import "TouchUIView.h"
#import "ZJSwitch.h"
#import "EditUserInfoView.h"
#import "ModifyPasswordView.h"
@interface FourthViewController ()

@end

@implementation FourthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // 1 create top view
    [self CreateTopView];
    
    // 1 create content view
    [self CreateContentView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 1 create top view
#pragma mark - 创建顶部视图
-(void)CreateTopView{
    UIView *payView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 64)];
    payView.backgroundColor = [UIFont MainColor];
    [self.view addSubview:payView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, 40)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont titleFont];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"设置";
    [payView addSubview:titleLabel];
}


// 1 create content view
#pragma mark - 创建顶部视图
-(void)CreateContentView{
    containerView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-110)];
    containerView.delegate = self;
    containerView.showsVerticalScrollIndicator = FALSE;
    containerView.contentSize = CGSizeMake(self.view.frame.size.width, 800);
    [self.view addSubview:containerView];
    
    [self UserInfoView];
    
    [self MapView];
    
    [self ChatView];
    
    [self NotificationView];
    
    [self OtherView];
}

-(void)UserInfoView{
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"个人信息",@"修改密码",nil];
    
    UIView *TitleView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, (titeArr.count+1)*50)];
    TitleView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:TitleView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontName:12];
    titleLabel.textColor = [UIColor colorWithRed:100.0/255 green:100.0/255 blue:100.0/255 alpha:1.0f];
    titleLabel.text = @"我";
    [TitleView addSubview:titleLabel];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,49.5, self.view.frame.size.width-10, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [TitleView addSubview:line];
    
    
    
    for (int i = 0; i < titeArr.count; i++) {
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(0,50+50*i,self.view.frame.size.width, 50)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[UIColor clearColor]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = i;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DIDSelect:)];
        [Unit addGestureRecognizer:singleTap];
        [TitleView addSubview:Unit];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 40)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [titeArr objectAtIndex:i];
        [Unit addSubview:titleLabel];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,0, Unit.frame.size.width-10, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0f];
        [Unit addSubview:line];
    }
    
}

-(void)MapView{
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"离线地图下载",nil];
    
    UIView *TitleView=[[UIView alloc] initWithFrame:CGRectMake(0,150, self.view.frame.size.width, (titeArr.count+1)*50)];
    TitleView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:TitleView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontName:12];
    titleLabel.textColor = [UIColor colorWithRed:100.0/255 green:100.0/255 blue:100.0/255 alpha:1.0f];
    titleLabel.text = @"地图管理";
    [TitleView addSubview:titleLabel];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,49.5, self.view.frame.size.width-10, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [TitleView addSubview:line];
    
    for (int i = 0; i < titeArr.count; i++) {
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(0,50+50*i,self.view.frame.size.width, 50)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[UIColor clearColor]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = 2+i;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DIDSelect:)];
        [Unit addGestureRecognizer:singleTap];
        [TitleView addSubview:Unit];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [titeArr objectAtIndex:i];
        [Unit addSubview:titleLabel];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,0, Unit.frame.size.width-10, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0f];
        [Unit addSubview:line];
    }
}

-(void)ChatView{
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"使用扬声器播放语音",@"通讯录黑名单",nil];
    
    UIView *TitleView=[[UIView alloc] initWithFrame:CGRectMake(0,250, self.view.frame.size.width, (titeArr.count+1)*50)];
    TitleView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:TitleView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontName:12];
    titleLabel.textColor = [UIColor colorWithRed:100.0/255 green:100.0/255 blue:100.0/255 alpha:1.0f];
    titleLabel.text = @"聊天设置";
    [TitleView addSubview:titleLabel];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,49.5, self.view.frame.size.width-10, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [TitleView addSubview:line];
    
    for (int i = 0; i < titeArr.count; i++) {
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(0,50+50*i,self.view.frame.size.width, 50)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[UIColor clearColor]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = 2+i;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DIDSelect:)];
        [Unit addGestureRecognizer:singleTap];
        [TitleView addSubview:Unit];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [titeArr objectAtIndex:i];
        [Unit addSubview:titleLabel];
        if (i == 0) {
            ZJSwitch *switch0 = [[ZJSwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, 12, 40, 15)];
            switch0.backgroundColor = [UIColor clearColor];
            [switch0 addTarget:self action:@selector(handleSwitchEvent:) forControlEvents:UIControlEventValueChanged];
            [Unit addSubview:switch0];
        }
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,0, Unit.frame.size.width-10, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0f];
        [Unit addSubview:line];
    }
}

-(void)NotificationView{
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"接收新消息通知",@"声音",@"震动",nil];
    
    UIView *TitleView=[[UIView alloc] initWithFrame:CGRectMake(0,400, self.view.frame.size.width, (titeArr.count+1)*50)];
    TitleView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:TitleView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontName:12];
    titleLabel.textColor = [UIColor colorWithRed:100.0/255 green:100.0/255 blue:100.0/255 alpha:1.0f];
    titleLabel.text = @"新消息提醒";
    [TitleView addSubview:titleLabel];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,49.5, self.view.frame.size.width-10, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [TitleView addSubview:line];
    
    for (int i = 0; i < titeArr.count; i++) {
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(0,50+50*i,self.view.frame.size.width, 50)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[UIColor clearColor]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = 2+i;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DIDSelect:)];
        [Unit addGestureRecognizer:singleTap];
        [TitleView addSubview:Unit];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [titeArr objectAtIndex:i];
        [Unit addSubview:titleLabel];
        
        ZJSwitch *switch0 = [[ZJSwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, 12, 40, 15)];
        switch0.backgroundColor = [UIColor clearColor];
        [switch0 addTarget:self action:@selector(handleSwitchEvent:) forControlEvents:UIControlEventValueChanged];
        [Unit addSubview:switch0];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,0, Unit.frame.size.width-10, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0f];
        [Unit addSubview:line];
    }
}

-(void)handleSwitchEvent:(id)sender{
    
}

-(void)OtherView{
    NSArray *titeArr =[[NSArray alloc] initWithObjects:@"信息反馈",@"检查更新(当前版本V1.0)",@"关于",nil];
    
    UIView *TitleView=[[UIView alloc] initWithFrame:CGRectMake(0,600, self.view.frame.size.width, (titeArr.count+1)*50)];
    TitleView.backgroundColor = [UIColor clearColor];
    [containerView addSubview:TitleView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont fontName:12];
    titleLabel.textColor = [UIColor colorWithRed:100.0/255 green:100.0/255 blue:100.0/255 alpha:1.0f];
    titleLabel.text = @"其他";
    [TitleView addSubview:titleLabel];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,49.5, self.view.frame.size.width-10, 0.5)];
    line.backgroundColor = [UIColor grayColor];
    [TitleView addSubview:line];
    
    for (int i = 0; i < titeArr.count; i++) {
        TouchUIView *Unit=[[TouchUIView alloc] initWithFrame:CGRectMake(0,50+50*i,self.view.frame.size.width, 50)];
        Unit.dragEnable = YES;
        [Unit setBackgroundColor:[UIColor clearColor]];
        Unit.userInteractionEnabled = YES;
        Unit.tag = 2+i;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DIDSelect:)];
        [Unit addGestureRecognizer:singleTap];
        [TitleView addSubview:Unit];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 50)];
        titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = [UIFont fontName:14];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [titeArr objectAtIndex:i];
        [Unit addSubview:titleLabel];
        
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(5,0, Unit.frame.size.width-10, 0.5)];
        line.backgroundColor = [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0f];
        [Unit addSubview:line];
    }
}


-(void)DIDSelect:(id)sender{
    UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
    int index = (int)[singleTap view].tag;
    switch (index) {
        case 0:{
            EditUserInfoView *menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditUserInfoView"];
            [self.navigationController pushViewController:(UIViewController*)menuVc animated:YES];
        }
            break;
        case 1:{
            ModifyPasswordView *menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ModifyPasswordView"];
            [self.navigationController pushViewController:(UIViewController*)menuVc animated:YES];
        }
            break;
        default:
            break;
    }
   
}
@end
