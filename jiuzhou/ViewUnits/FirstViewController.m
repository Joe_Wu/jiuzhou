//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "FirstViewController.h"
#import "ColorFont.h"
#import "MessageCellLayout.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:240.0/255 green:240.0/255 blue:240.0/255 alpha:1.0f];
    dataArray = [[NSMutableArray alloc]init];
    searchResults = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< 10; i++) {
        [dataArray addObject:[NSString stringWithFormat:@"消息%d",i]];
    }
    // 1 create top view
    [self CreateTopView];
    
    // 2 search and table view
    [self CreateSearchAndtable];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 1 create top view
#pragma mark - 创建顶部视图
-(void)CreateTopView{
    UIView *payView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 64)];
    payView.backgroundColor = [UIFont MainColor];
    [self.view addSubview:payView];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-20, 40)];
    titleLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont titleFont];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"消息";
    [payView addSubview:titleLabel];
}

// 2 search and table view
#pragma mark - 创建搜索和列表视图
-(void)CreateSearchAndtable{
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 40)];
    mySearchBar.delegate = self;
    [mySearchBar setPlaceholder:@"搜索消息"];
    float version = [[[ UIDevice currentDevice ] systemVersion ] floatValue ];
    if ([ mySearchBar respondsToSelector : @selector (barTintColor)]) {
        float  iosversion7_1 = 7.1 ;
        if (version >= iosversion7_1){
            //iOS7.1
            [[[[ mySearchBar . subviews objectAtIndex : 0 ] subviews ] objectAtIndex : 0 ] removeFromSuperview ];
            [ mySearchBar setBackgroundColor :[ UIColor whiteColor ]];
            
        }else{
            //iOS7.0
            [ mySearchBar setBarTintColor :[ UIColor whiteColor ]];
            [ mySearchBar setBackgroundColor :[ UIColor whiteColor ]];
        }
    }else{
        //iOS7.0 以下
        [[ mySearchBar . subviews objectAtIndex : 0 ] removeFromSuperview ];
        [ mySearchBar setBackgroundColor :[ UIColor whiteColor ]];
    }
    [self.view addSubview:mySearchBar];
//    [mySearchBar becomeFirstResponder];
    
    searchDisplayController = [[UISearchDisplayController alloc]initWithSearchBar:mySearchBar contentsController:self];
    searchDisplayController.active = NO;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 104,self.view.frame.size.width,self.view.frame.size.height-64) style:UITableViewStyleGrouped];
    _tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, 0.01)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //    _tableView.separatorColor = [UIColor grayColor];
    _tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tableView];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
}
#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    MessageCellLayout *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCellLayout" owner:self options:nil] lastObject];
    }
    
    UILabel *distanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 20, self.view.frame.size.width-30, 20)];
    distanceLabel.backgroundColor = [UIColor clearColor]; //可以去掉背景色
    distanceLabel.textAlignment = NSTextAlignmentLeft;
    distanceLabel.font = [UIFont fontName:16];
    distanceLabel.textColor = [UIFont GrayTextColor];
    distanceLabel.text = [NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexPath.row]];
    [cell.CellBackView addSubview:distanceLabel];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
// 分割线从最左边绘制
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    searchResults = [[NSMutableArray alloc]init];
    if (mySearchBar.text.length>0&&![ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
        for (int i=0; i<dataArray.count; i++) {
            if ([ChineseInclude isIncludeChineseInString:dataArray[i]]) {
                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:dataArray[i]];
                NSRange titleResult=[tempPinYinStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [searchResults addObject:dataArray[i]];
                }
                //                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:dataArray[i]];
                //                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
                //                if (titleHeadResult.length>0) {
                //                    [searchResults addObject:_ShopArray[i]];
                //                }
            }
            else {
                NSRange titleResult=[dataArray[i] rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [searchResults addObject:dataArray[i]];
                }
            }
        }
    } else if (mySearchBar.text.length>0&&[ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
        for (NSString *tempStr in dataArray) {
            NSRange titleResult=[tempStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
            if (titleResult.length>0) {
                [searchResults addObject:tempStr];
            }
        }
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode sbar (UISearchBar)
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    UIColor *desiredColor = [UIColor colorWithRed:212.0/255.0 green:237.0/255.0 blue:187.0/255.0 alpha:1.0];
    for (UIView *subView in [[ mySearchBar . subviews objectAtIndex : 0 ] subviews ]){
        if([subView isKindOfClass:[UIButton class]]){
            [(UIButton *)subView setTintColor:desiredColor];
            [(UIButton *)subView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
}


@end
