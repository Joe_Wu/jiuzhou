//
//  ViewController.h
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimateTabbar.h"

@interface ViewController : UIViewController<AnimateTabbarDelegate>

@property (weak, nonatomic) IBOutlet UIView *childView;
@end

