//
//  HSCButton.m
//  AAAA
//
//  Created by zhangmh on 12-7-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ServiceAPI.h"
#import "Constants.h"
#import "HttpUnits.h"

@implementation ServiceAPI

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        self.backgroundColor = [UIColor clearColor];
        
//        XYSpriteView *tmpSprite = [[XYSpriteView alloc] initWithFrame:CGRectMake((frame.size.width-[loadWith intValue])/2, (frame.size.height-[loadHight intValue])/2, [loadWith intValue], [loadHight intValue])];
//        tmpSprite.firstImgIndex = 1;
//        [tmpSprite formatImg:@"ani%d.png" count:4 repeatCount:0];
//        [tmpSprite showImgWithIndex:0];
//        tmpSprite.delegate = self;
//        [[XYSpriteHelper sharedInstance].sprites setObject:tmpSprite forKey:@"a"];
//        [self addSubview:tmpSprite];
//        [[XYSpriteHelper sharedInstance] startAllSprites];
//        
//        [[XYSpriteHelper sharedInstance] startTimer];
        
        UIActivityIndicatorView  *act=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [act setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
        [self addSubview:act];
        
        [act startAnimating];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)SetGetRequest{
    NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                 selector:@selector(GET)
                                                   object:nil];
    [myThread start];
}

-(void)SetPostRequest:(NSMutableDictionary*)dic{
    self.dic = dic;
    NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                 selector:@selector(POST)
                                                   object:nil];
    [myThread start];
}

-(void)SetPutRequest:(NSMutableDictionary*)dic{
    self.dic = dic;
    NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                 selector:@selector(PUT)
                                                   object:nil];
    [myThread start];
}

-(void)GET{
    NSMutableDictionary *responseDictionary = [HttpUnits GetRequestToService:[NSString stringWithFormat:@"%@%@",ApiUrl,_url]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",ApiUrl,_url]);
    
    if (responseDictionary != nil) {
        [self.delegate RequestSuccess:responseDictionary];
    }else{
        [self.delegate RequestFaild:@""];
    }
}

-(void)POST{
    NSMutableDictionary *responseDictionary = [HttpUnits PostJsonToService:[NSString stringWithFormat:@"%@%@",ApiUrl,_url] RequestJson:self.dic];
    if (responseDictionary != nil) {
        [self.delegate RequestSuccess:responseDictionary];
    }else{
        [self.delegate RequestFaild:@""];
    }
}

-(void)PUT{
    NSMutableDictionary *responseDictionary = [HttpUnits PutRequestToService:[NSString stringWithFormat:@"%@%@",ApiUrl,_url] RequestJson:_dic];
    if (responseDictionary != nil) {
        [self.delegate RequestSuccess:responseDictionary];
    }else{
        [self.delegate RequestFaild:@""];
    }
}

@end
