//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIViewController *firstViewController;
UIViewController *secondViewController;
UIViewController *thirdViewController;
UIViewController *fourthViewController;
UIViewController *currentViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [super.navigationController setNavigationBarHidden:TRUE animated:TRUE];

    AnimateTabbarView *tabbar=[[AnimateTabbarView alloc]initWithFrame:self.view.frame];
    tabbar.delegate=self;
    [self.view addSubview:tabbar];
    
    firstViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"FirstViewController"];
    [self addChildViewController:firstViewController];
    
    secondViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    [self addChildViewController:secondViewController];
    
    thirdViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"ThirdViewController"];
    [self addChildViewController:thirdViewController];
    
    fourthViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"FourthViewController"];
    [self addChildViewController:fourthViewController];
    
    [self.childView addSubview:firstViewController.view];
    currentViewController=firstViewController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// callback
int g_flags=1;
-(void)FirstBtnClick{
    
    if(g_flags==1)
        return;
    [self transitionFromViewController:currentViewController toViewController:firstViewController duration:0 options:0 animations:^{
    }  completion:^(BOOL finished) {
        currentViewController=firstViewController;
        g_flags=1;
        
    }];
}
-(void)SecondBtnClick{
    if(g_flags==2)
        return;
    [self transitionFromViewController:currentViewController toViewController:secondViewController duration:0 options:0 animations:^{
    }  completion:^(BOOL finished) {
        currentViewController=secondViewController;
        g_flags=2;
        
    }];
    
}
-(void)ThirdBtnClick{
    if(g_flags==3)
        return;
    [self transitionFromViewController:currentViewController toViewController:thirdViewController duration:0 options:0 animations:^{
    }  completion:^(BOOL finished) {
        currentViewController=thirdViewController;
        g_flags=3;
        
    }];
}
-(void)FourthBtnClick{
    if(g_flags==4)
        return;
    [self transitionFromViewController:currentViewController toViewController:fourthViewController duration:0 options:0 animations:^{
    }  completion:^(BOOL finished) {
        currentViewController=fourthViewController;
        g_flags=4;
        
    }];
}

@end
