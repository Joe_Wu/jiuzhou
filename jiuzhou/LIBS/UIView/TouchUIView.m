//
//  HSCButton.m
//  AAAA
//
//  Created by zhangmh on 12-7-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TouchUIView.h"

@implementation TouchUIView

@synthesize dragEnable;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        self.backgroundColor = [UIColor redColor];
    }
    return self;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!dragEnable) {
        return;
    }
    UITouch *touch = [touches anyObject];
    
//    beginPoint = [touch locationInView:self];
//    self.backgroundColor = [UIColor colorWithRed:228.0/255 green:228.0/255 blue:228.0/255 alpha:1.0f];
    self.alpha = 0.2;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!dragEnable) {
        return;
    }

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    self.backgroundColor = [UIColor clearColor];
    self.alpha = 1.0;
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
//    self.backgroundColor = [UIColor blackColor];
    self.alpha = 1.0;
}

@end
