//
//  UIViewController+View1.h
//  Libs
//
//  Created by Yang Joe on 12/18/14.
//  Copyright (c) 2014 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "view2.h"
@interface View1:UIViewController<view2Delegate>

@property (weak, nonatomic) IBOutlet UITextField *textFiled2;

- (IBAction)Done2:(id)sender;
@end
