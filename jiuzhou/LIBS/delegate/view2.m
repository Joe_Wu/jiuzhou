//
//  UIViewController+view2.m
//  Libs
//
//  Created by Yang Joe on 12/18/14.
//  Copyright (c) 2014 Yang Joe. All rights reserved.
//

#import "view2.h"

@interface view2 ()

@end

@implementation view2

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)Done:(id)sender {
    // 发送代理，并把文本框中的值传过去
    [self.delegate changeValue:self.textfiled.text];
    [self.navigationController popViewControllerAnimated:YES];
    // 退出当前窗口
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
