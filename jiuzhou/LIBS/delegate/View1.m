//
//  UIViewController+View1.m
//  Libs
//
//  Created by Yang Joe on 12/18/14.
//  Copyright (c) 2014 Yang Joe. All rights reserved.
//

#import "View1.h"

@interface View1 ()

@end

@implementation View1

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)Done2:(id)sender {
    // 设置代理到此Controller
    view2* menuVc = [self.storyboard instantiateViewControllerWithIdentifier:@"view2"];
    
    menuVc.delegate = self;

    [self.navigationController pushViewController:(UIViewController*)menuVc animated:YES];

}

/**
 此方为必须实现的协议方法，用来传值
 */
- (void)changeValue:(NSString *)value{
    // 改变UILabel的值
    self.textFiled2.text = value;
}



@end
