//
//  UIViewController+view2.h
//  Libs
//
//  Created by Yang Joe on 12/18/14.
//  Copyright (c) 2014 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 定义协议，用来实现传值代理
 */
@protocol view2Delegate <NSObject>
/**
 此方为必须实现的协议方法，用来传值
 */
- (void)changeValue:(NSString *)value;

@end

@interface view2:UIViewController
/**
 此处利用协议来定义代理
 */
@property (nonatomic, unsafe_unretained) id<view2Delegate> delegate;

/**
 这个文本框中的值可以自己随意改变。
 当点击“我变变变！”按钮后，它里边的值会回传到调用它的WViewController中
 */
@property (weak, nonatomic) IBOutlet UITextField *textfiled;
- (IBAction)Done:(id)sender;

@end
