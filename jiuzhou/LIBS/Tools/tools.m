//
//  tools.m
//  MemoryBox
//
//  Created by YangJoe on 10/28/13.
//
//

#import "tools.h"

@implementation tools
NSUserDefaults *userDefaultes;

// 使用 NSUserDefaults 保存数组
-(void)saveArray:(NSMutableArray*)array saveKey:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSArray * myarray = [[NSArray alloc] initWithArray:array];
    NSData *udObject = [NSKeyedArchiver archivedDataWithRootObject:myarray];
    [userDefaultes setObject:udObject forKey:key];
}

// 使用 NSUserDefaults 读取数组
-(NSMutableArray*)readArray:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSData *udObject = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:udObject];
    NSMutableArray *myArray2 =  [NSMutableArray arrayWithArray:myArray];
    return myArray2;
}

// 使用 NSUserDefaults 保存对象
-(void)saveObject:(NSObject*)object saveKey:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    [userDefaultes setObject:object forKey:key];
}

// 使用 NSUserDefaults 读取对象
-(NSObject*)readObkect:(NSString*)key{
    userDefaultes = [NSUserDefaults standardUserDefaults]; // 初始化 NSUserDefaults
    NSObject *object = [userDefaultes stringForKey:key];
    return object;
}

// 判断网络是否存在
+(BOOL)isExistenceNetwork {
    BOOL isExistenceNetwork = YES;
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork = NO;
            //NSLog(@"notReachable");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork = YES;
            //NSLog(@"WIFI");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork = YES;
            //NSLog(@"3G");
            break;
    }
    return isExistenceNetwork;
}

-(NSString*)CountDays{
    //获得系统当前时间
    NSDate *  senddate=[NSDate date];
    NSCalendar  * cal=[NSCalendar  currentCalendar];
    NSUInteger  unitFlags=NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit;
    NSDateComponents * conponent= [cal components:unitFlags fromDate:senddate];
    NSInteger nowMonth=[conponent month];
    NSInteger nowDay=[conponent day];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //获得上次登录时间
    NSString *lastmoth = [userDefaults stringForKey:@"countmonth"];
    NSString *lastday = [userDefaults stringForKey:@"countdays"];
    
    int oldmonth = [lastmoth intValue];
    int oldday = [lastday intValue];
    
    //计算两次的时间差
    int time = 30*(nowMonth - oldmonth) + (nowDay - oldday);
    NSString *countdays = [[NSString alloc]initWithFormat:@"%d",time];
    
    NSString *savelastday = [[NSString alloc]initWithFormat:@"%d",nowDay];
    NSString *savelastmonth = [[NSString alloc]initWithFormat:@"%d",nowMonth];
    
    if (time >= 3) {
        // 保存本次刷新的时间
        [userDefaults setObject:savelastmonth forKey:@"countmonth"];
        [userDefaults setObject:savelastday forKey:@"countdays"];
    }
    
    return countdays;
}

// DATEFORMAT TO DATE
+(NSString*)toDate:(NSString*)Chuo{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[Chuo longLongValue]];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDateStr1 = [dateFormatter1 stringFromDate:date];
    return currentDateStr1;
}
// DATEFORMAT TO DATE HOUR AND MINUTES
+(NSString*)toDateHour:(NSString*)Chuo{

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM-dd HH:mm:ss"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[Chuo longLongValue]];
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    return confromTimespStr;
}

+(NSString*)toDateFormat:(NSString*)dateString{

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:MM"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    
    NSDate* date = [formatter dateFromString:dateString];

    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    return timeSp;
}

+(NSString*)nowDate{
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter  alloc ]  init ];
    [formatter setDateFormat:@"YYYY-MM-dd HH:MM"];
    NSString *todayTime = [formatter stringFromDate:today];
    return todayTime;
}

+(NSString*)nowDateInDay{
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter  alloc ]  init ];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *todayTime = [formatter stringFromDate:today];
    return todayTime;
}


@end
