//
//  UIViewController+QRScanView.h
//  Libs
//
//  Created by Yang Joe on 12/19/14.
//  Copyright (c) 2014 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeReaderDelegate.h"
@interface QRScanView:UIViewController<QRCodeReaderDelegate>

- (IBAction)scan:(id)sender;
@end
