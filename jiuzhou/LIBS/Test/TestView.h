//
//  ViewController.h
//  TUOLS
//
//  Created by Yang Joe on 2/16/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestView : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSArray *functionArray;
}

@property(nonatomic, retain) UITableView* tableView;
@end

