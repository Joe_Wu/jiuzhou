//
//  ViewController.m
//  TUOLS
//
//  Created by Yang Joe on 2/16/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "TestView.h"
#import "HttpUnits.h"

@interface TestView ()

@end

@implementation TestView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
        
    functionArray = [NSArray arrayWithObjects:@"网络请求封装",@"加载请求封装",@"列表封装",@"及时通讯封装",@"提示框",@"短信验证",@"代理机制",@"读取plist数据",@"存储plist到沙盒",@"二维码扫描", nil];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = [UIColor grayColor];
    [self.view addSubview:_tableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ------------------table view --------------start
#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return functionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdetify = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (cell!=nil) {
        cell = nil;
    }
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdetify];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [functionArray objectAtIndex:indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self httpRequestTest];
            break;
    }
}
//------------------table view------------------end


// HTTP REQUEST TEST
-(void)httpRequestTest{
    NSString *url = @"http://115.28.162.31:8000/getCities";
    NSMutableDictionary *responseDictionary = [HttpUnits GetRequestToService:url];
    NSLog(@"%@",responseDictionary);
}

@end
