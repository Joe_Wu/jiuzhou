//
//  ViewController.m
//  jiuzhou
//
//  Created by Yang Joe on 4/2/15.
//  Copyright (c) 2015 Yang Joe. All rights reserved.
//

#import "LoginController.h"
#import "ViewController.h"
@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithRed:67.0/255.0 green:83.0/255.0 blue:109.0/255.0 alpha:1.0f];
    [self UserLoginInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 0 User Login View
-(void)UserLoginInfo{
    
    UserName = [[UITextField alloc] initWithFrame:CGRectMake(30.0f, self.view.frame.size.height/4+64, self.view.frame.size.width-60, 50.0f)];
    [UserName setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    UserName.backgroundColor = [UIColor whiteColor];
    UserName.layer.cornerRadius = 3.0f;
    UserName.placeholder = @"请输入手机号";
    UserName.delegate =self;
    UserName.keyboardType = UIKeyboardTypeDecimalPad;//键盘类型为数字
    UserName.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    UserName.autocorrectionType = UITextAutocorrectionTypeNo;
    UserName.autocapitalizationType = UITextAutocapitalizationTypeNone;
    UserName.returnKeyType = UIReturnKeyDone;
    UserName.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
    [self.view addSubview:UserName];
    
    Password = [[UITextField alloc] initWithFrame:CGRectMake(30, self.view.frame.size.height/4+124, self.view.frame.size.width-60, 50.0f)];
    [Password setBorderStyle:UITextBorderStyleRoundedRect]; //外框类型
    Password.backgroundColor = [UIColor whiteColor];
    Password.layer.cornerRadius = 3.0f;
    Password.secureTextEntry = YES; //密码
    Password.placeholder = @"请输入密码";
    Password.delegate =self;
    Password.font = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    Password.autocorrectionType = UITextAutocorrectionTypeNo;
    Password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    Password.returnKeyType = UIReturnKeyDone;
    Password.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:Password];
    
    UIButton *forgetPassword = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    forgetPassword.frame = CGRectMake(Password.frame.size.width-40, 10, 30.0, 30.0);
    forgetPassword.backgroundColor = [UIColor clearColor];
    [forgetPassword setBackgroundImage:[UIImage imageNamed:@"forget_password_icon.png"] forState:UIControlStateNormal];
    [forgetPassword addTarget:self action:@selector(ForgetPassword:) forControlEvents:UIControlEventTouchUpInside];
    [Password addSubview:forgetPassword];
    
    UIButton *RegirestBut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    RegirestBut.frame = CGRectMake(30, self.view.frame.size.height/4+210, (self.view.frame.size.width-60)/2-5, 50.0f);
    RegirestBut.backgroundColor = [UIColor colorWithRed:101.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1.0f];
    RegirestBut.layer.cornerRadius = 3.0f;
    RegirestBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:18.0f];
    [RegirestBut setTitle:@"注  册" forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [RegirestBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [RegirestBut addTarget:self action:@selector(Regirest:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:RegirestBut];
    
    UIButton *LoginBut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    LoginBut.frame = CGRectMake(self.view.frame.size.width/2+5, self.view.frame.size.height/4+210, (self.view.frame.size.width-60)/2-5, 50.0f);
    LoginBut.backgroundColor = [UIColor colorWithRed:101.0/255.0 green:202.0/255.0 blue:255.0/255.0 alpha:1.0f];
    LoginBut.layer.cornerRadius = 3.0f;
    LoginBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Book" size:18.0f];
    [LoginBut setTitle:@"登  录" forState:UIControlStateNormal];
    [LoginBut addTarget:self action:@selector(Login:) forControlEvents:UIControlEventTouchUpInside];
    [LoginBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [LoginBut setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
    [self.view addSubview:LoginBut];
    
    //hide the virtual keyboad
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
}
// hide virtual keyboard
-(void)viewTapped:(UITapGestureRecognizer*)tapGr{
    [UserName resignFirstResponder];
    [Password resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UserName resignFirstResponder];
    [Password resignFirstResponder];
    return YES;
}

-(void)ForgetPassword:(id)sender{
    
}
-(void)Regirest:(id)sender{
    
}
-(void)Login:(id)sender{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *welcomeBoardVC =
    [storyBoard instantiateViewControllerWithIdentifier:@"ViewController"];
    self.navController = [[KKNavigationController alloc] initWithRootViewController:welcomeBoardVC];
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
}
@end
